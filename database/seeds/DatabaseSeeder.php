<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Custom\Helpers;
use App\Javascript;
use App\Jimages;
use App\CssFonts;
use App\Jtext;
use App\NavItems;
use App\NavBrand;
use App\About;
use App\Locations;
use App\Menu;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        $this->call('JavascriptTableSeeder');
        $this->call('JimagesTableSeeder');
		$this->call('CSSFontsTableSeeder');
        $this->call('JtextTableSeeder');
        $this->call('NavItemsTableSeeder');
        $this->call('NavBrandTableSeeder');
        $this->call('AboutTableSeeder');
        $this->call('LocationsTableSeeder');
        $this->call('MenuTableSeeder');
	}
}

	class JavascriptTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
                	$uuid = ($helpers->getUUID());

                	Javascript::create([
                	 'uuid' => $uuid,
            		 'preloader_delay' => 500,
            		 'preloader_fade_out' => 'slow',
                     'ss_image_duration' => 5000,
                     'ss_image_fade' => 600,
                     'v_background_mp4' => "/storage/video/newVideo.mp4",
                     'v_background_webm' => "/storage/video/newVideo.webm",
                     'v_background_ogg' => "/storage/video/newVideo.ogg",
                     'intro_text_pagination_speed' => 600,
                     'intro_text_autoplay' => 5000,
                     'testimonial_text_pagination_speed' => 600,
                     'testimonial_text_autoplay' => 3000,
                     'latitude' => 43.161030,
                     'longitude' => -77.610924,
                     'map_marker' => "marker.png",
                     'map_zoom' => 15
            		]);
        }

    }

	class JimagesTableSeeder extends Seeder {

	    public function run()
	    {
	        

	        $helpers = new \App\Custom\Helpers;
	            	$uuid = ($helpers->getUUID());

	            	Jimages::create([
        				'uuid' => $uuid,
        				'image_order' => 1,
        				'ss_image' => '/jsimages/Pizza3.jpg',
        				]);

        			Jimages::create([
        				'uuid' => $uuid,
        				'image_order' => 2,
        				'ss_image' => '/jsimages/Pizza4.jpg',
        				]);

        			Jimages::create([
        				'uuid' => $uuid,
        				'image_order' => 3,
        				'ss_image' => '/jsimages/Pizza1.jpg',
        				]);

                    Jimages::create([
                        'uuid' => $uuid,
                        'image_order' => 4,
                        'ss_image' => '/jsimages/Pizza5.jpg',
                        ]);

                    Jimages::create([
                        'uuid' => $uuid,
                        'image_order' => 5,
                        'ss_image' => '/jsimages/Pizza2.jpg',
                        ]);
	    }

	}

    class CSSFontsTableSeeder extends Seeder {

        public function run()
        {
            
            $helpers = new \App\Custom\Helpers;
                    $uuid = ($helpers->getUUID());

                CssFonts::create([
                 'uuid' => $uuid,
                 'body_font' => 'Arimo',
                 'body_font_size' => '16',
                 'body_font_color' => 'rgba(64,64,64,1)',
                 'intro_slider_font' => 'Rhodium+Libre',
                 'intro_slider_font_size' => '44',
                 'intro_slider_font_color' => 'rgba(255,255,255,1)',
                 'title_font' => 'Crimson+Text',
                 'title_font_size' => '54',
                 'title_font_color' => '#000',
                 'subtitle_font' => 'Libre+Baskerville',
                 'subtitle_font_size' => '16',
                 'subtitle_font_color' => 'rgba(64,64,64,1)'
                ]);
        }

    }

    class JtextTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
            $uuid = ($helpers->getUUID());

                Jtext::create([
                    'uuid' => $uuid,
                    'slide_order' => 1,
                    'slide_type' => 'image',
                    'slide_text' => '',
                    'image_path' => '/slideimages/pizza-snapp-splash.png',
                    ]);

                Jtext::create([
                    'uuid' => $uuid,
                    'slide_order' => 2,
                    'slide_type' => 'text',
                    'slide_text' => 'Authentic Italian Wood Fired Pizza.',
                    'image_path' => '',
                    ]);

                Jtext::create([
                    'uuid' => $uuid,
                    'slide_order' => 3,
                    'slide_type' => 'text',
                    'slide_text' => 'Taste the Old-World Tradition.',
                    'image_path' => '',
                    ]);

                Jtext::create([
                    'uuid' => $uuid,
                    'slide_order' => 4,
                    'slide_type' => 'text',
                    'slide_text' => 'Impeccable European Service.',
                    'image_path' => '',
                    ]);


        }

    }

    class NavItemsTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
            $uuid = ($helpers->getUUID());

                NavItems::create([
                    'uuid' => $uuid,
                    'nav_order' => 1,
                    'nav_name' => 'Home',
                    'nav_path' => '#home',
                    ]);

                NavItems::create([
                    'uuid' => $uuid,
                    'nav_order' => 2,
                    'nav_name' => 'About Us',
                    'nav_path' => '#about',
                    ]);

                NavItems::create([
                    'uuid' => $uuid,
                    'nav_order' => 3,
                    'nav_name' => 'Locations',
                    'nav_path' => '#locations',
                    ]);

                NavItems::create([
                    'uuid' => $uuid,
                    'nav_order' => 5,
                    'nav_name' => 'Menu',
                    'nav_path' => '#menu',
                    ]);

                NavItems::create([
                    'uuid' => $uuid,
                    'nav_order' => 6,
                    'nav_name' => 'Map',
                    'nav_path' => '#map-section',
                    ]);

                NavItems::create([
                    'uuid' => $uuid,
                    'nav_order' => 7,
                    'nav_name' => 'Contact Us',
                    'nav_path' => '#contact',
                    ]);


        }

    }

    class NavBrandTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
            $uuid = ($helpers->getUUID());

                NavBrand::create([
                    'uuid' => $uuid,
                    'nav_brand_type' => 'image',
                    'nav_image' => '/brandimages/pizza-snapp-logo.png',
                    'nav_text' => 'Pizza Snapp',
                    ]);

        }

    }

    class AboutTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
            $uuid = ($helpers->getUUID());

                About::create([
                    'uuid' => $uuid,
                    'about_id' => '#about',
                    'about_title' => 'Our Story',
                    'about_subtitle' => 'Family owned and operated since 2009',
                    'about_text' => 'The PizzaSnapp website, Mobile App and this All-New Landing Page was setup as a demonstration of the OrderSnapp All-in-One Solution. OrderSnapp Landing Pages are built using responsive design so they look great on any device. In addition to maximizing your SEO, custom landing pages utilize Facebook and Twitter tags, so anytime someone shares your sites content it will always look exactly how you want. Go to OrderSnapp.com for more details',
                    'about_divider_image' => 'assets/images/divider-top.png',
                    'callout_background_image' => 'assets/images/callout-4.jpg',
                    'callout_divider_image' => 'assets/images/divider-top.svg',
                    'callout_text' => 'Authentic Italian Wood Fired Pizza.'
                    ]);

        }

    }

    class LocationsTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
            $uuid = ($helpers->getUUID());

                Locations::create([
                    'uuid' => $uuid,
                    'location_id' => '#locations',
                    'location_title' => 'Locations',
                    'location_subtitle' => 'Serving the city of Rochester and surrounding locations.',
                    'location_header' => 'Downtown',
                    'location_name' => 'PizzaSnapp Downtown',
                    'location_address' => '120 East Ave, Suite 325',
                    'location_city' => 'Rochester',
                    'location_state' => 'NY',
                    'location_zip' => '14604',
                    'location_phone' => '1-888-402-6863',
                    'location_divider_image' => 'assets/images/divider-down.svg',
                    'callout_background_image' => 'assets/images/callout-1.jpg',
                    'callout_divider_image' => 'assets/images/divider-top.svg',
                    'callout_text' => 'Impeccable European Service.'
                    ]);

                 Locations::create([
                    'uuid' => $uuid,
                    'location_header' => 'East Side',
                    'location_name' => 'PizzaSnapp East',
                    'location_address' => '349 W Commercial St',
                    'location_city' => 'East Rochester',
                    'location_state' => 'NY',
                    'location_zip' => '14445',
                    'location_phone' => '1-585-555-1212'
                    ]);

                 Locations::create([
                    'uuid' => $uuid,
                    'location_header' => 'West Side',
                    'location_name' => 'PizzaSnapp West',
                    'location_address' => '100 Paddy Creek Cir',
                    'location_city' => 'Greece',
                    'location_state' => 'NY',
                    'location_zip' => '14615',
                    'location_phone' => '1-585-555-1212'
                    ]);

        }

    }

    class MenuTableSeeder extends Seeder {

        public function run()
        {
            

            $helpers = new \App\Custom\Helpers;
            $uuid = ($helpers->getUUID());

                Menu::create([
                    'uuid' => $uuid,
                    'menu_id' => '#menu',
                    'menu_title' => 'Menu',
                    'menu_subtitle' => 'Our Specialty Pizzas',
                    'menu_item_title' => 'Supreme Pizza',
                    'menu_item_detail' => 'Pepperoni / Sausage / Peppers / Onions / Olives',
                    'menu_item_price' => 16.95,
                    'menu_pdf_path' => 'assets/files/menu.pdf',
                    'menu_divider_image' => 'assets/images/divider-down.svg',
                    'callout_background_image' => 'assets/images/callout-1.jpg',
                    'callout_divider_image' => 'assets/images/divider-top.svg',
                    'callout_text' => 'Impeccable European Service.'
                    ]);

                Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Veggie Pizza',
                    'menu_item_detail' => 'Peppers / Onions / Olives / Mushrooms / Spinach',
                    'menu_item_price' => 14.55
                    ]);

                Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Philly Cheese Steak Pizza',
                    'menu_item_detail' => 'Shaved Steak / Onions / Cheeze Wiz',
                    'menu_item_price' => 18.55
                    ]);

                 Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Buffalo Chicken Pizza',
                    'menu_item_detail' => 'Chicken / Buffalo Sauce / Blue Cheese',
                    'menu_item_price' => 15.95
                    ]);

                 Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Chicken Bacon BBQ Pizza',
                    'menu_item_detail' => 'Bacon / Chicken / BBQ Sauce/ Cheddar Cheese',
                    'menu_item_price' => 16.45
                    ]);

                 Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Hawaiian Pizza',
                    'menu_item_detail' => 'Ham / Pineapple / Mozz',
                    'menu_item_price' => 14.55
                    ]);

                  Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Meat Lovers Pizza',
                    'menu_item_detail' => 'Bacon / Meatball / Sausage / Ham / Pepperoni',
                    'menu_item_price' => 18.55
                    ]);

                  Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Thin New York Style',
                    'menu_item_detail' => 'Meat Sauce / Mozz / Fresh Basil',
                    'menu_item_price' => 14.95
                    ]);

                  Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'Greek Pizza',
                    'menu_item_detail' => 'Artichoke / Red Onion / Kalamata Olives / Feta',
                    'menu_item_price' => 16.95
                    ]);

                  Menu::create([
                    'uuid' => $uuid,
                    'menu_item_title' => 'White Pizza',
                    'menu_item_detail' => 'White Garlic Sauce / Mozz',
                    'menu_item_price' => 14.55
                    ]);

        }

    }