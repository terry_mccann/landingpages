<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJavascriptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('javascript', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('preloader_delay');
            $table->enum('preloader_fade_out', ['slow', 'fast']);
            $table->integer('ss_image_duration');
            $table->integer('ss_image_fade');
            $table->string('v_background_mp4');
            $table->string('v_background_webm');
            $table->string('v_background_ogg');
            $table->integer('intro_text_pagination_speed');
            $table->integer('intro_text_autoplay');
            $table->integer('testimonial_text_pagination_speed');
            $table->integer('testimonial_text_autoplay');
            $table->decimal('latitude', 10, 8);
            $table->decimal('longitude', 11, 8);
            $table->string('map_marker');
            $table->integer('map_zoom');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('javascript');
	}

}
