<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('about', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid');
			$table->boolean('enabled')->default(1);
			$table->string('about_id');
			$table->string('about_title');
			$table->string('about_subtitle');
			$table->text('about_text');
			$table->string('about_divider_image');
 			$table->string('callout_background_image');
 			$table->string('callout_divider_image');
 			$table->string('callout_text');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('about');
	}

}
