<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCssFontsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('css_fonts', function (Blueprint $table) {
			$table->increments('id');
            $table->string('uuid');
            $table->string('body_font');
            $table->string('body_font_size');
            $table->string('body_font_color');
            $table->string('intro_slider_font');
            $table->string('intro_slider_font_size');
            $table->string('intro_slider_font_color');
            $table->string('title_font');
            $table->string('title_font_size');
            $table->string('title_font_color');
            $table->string('subtitle_font');
            $table->string('subtitle_font_size');
            $table->string('subtitle_font_color');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('css_fonts');
	}

}
