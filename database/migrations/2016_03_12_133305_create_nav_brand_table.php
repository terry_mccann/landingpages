<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavBrandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nav_brand', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid');
			$table->enum('nav_brand_type', ['text', 'image']);
            $table->string('nav_image');
            $table->string('nav_text');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nav_brand');
	}

}
