<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu', function(Blueprint $table)
			{
				$table->increments('id');
				$table->string('uuid');
				$table->boolean('enabled')->default(1);
				$table->string('menu_id');
				$table->string('menu_title');
				$table->string('menu_subtitle');
				$table->string('menu_item_title');
				$table->text('menu_item_detail');
				$table->decimal('menu_item_price', 5, 2);
				$table->string('menu_pdf_path');
				$table->string('menu_divider_image');
	 			$table->string('callout_background_image');
	 			$table->string('callout_divider_image');
	 			$table->string('callout_text');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu');
	}

}
