<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nav_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid');
			$table->string('nav_order');
			$table->string('nav_name');
			$table->string('nav_path');
			$table->boolean('nav_enabled')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nav_items');
	}

}
