<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJtextsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('js_text', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid');
			$table->string('slide_order');
			$table->enum('slide_type', ['text', 'image']);
			$table->string('slide_text');
			$table->string('image_path');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('js_text');
	}

}
