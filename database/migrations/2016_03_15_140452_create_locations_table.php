<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
			{
				$table->increments('id');
				$table->string('uuid');
				$table->boolean('enabled')->default(1);
				$table->string('location_id');
				$table->string('location_title');
				$table->string('location_subtitle');
				$table->string('location_header');
				$table->string('location_name');
				$table->string('location_address');
				$table->string('location_city');
				$table->string('location_state');
				$table->string('location_zip');
				$table->string('location_phone');
				$table->string('location_divider_image');
	 			$table->string('callout_background_image');
	 			$table->string('callout_divider_image');
	 			$table->string('callout_text');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
