# OrderSnapp Landing Pages Manager


OrderSnapp Landing Pages are built using responsive design so they look great on any device. In addition to maximizing your SEO, custom landing pages utilize Facebook and Twitter tags, so anytime someone shares your sites content it will always look exactly how you want. 

Our landing page management system allows you to easily customize every aspect of a customers landing page. Upload custom images, update sections with custom text, adjust slider speeds, add edit and delete menu items.

## Official Documentation

Documentation for the framework can be found on the [OrderSnapp website](http://ordersnapp.com).