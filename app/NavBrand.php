<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NavBrand extends Model {

	protected $table = 'nav_brand';

}