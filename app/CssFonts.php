<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CssFonts extends Model {

	protected $table = 'css_fonts';

	protected $fillable = ['body_font',
							'body_font_size',
							'body_font_color',
							'intro_slider_font',
							'intro_slider_font_size',
							'intro_slider_font_color',
							'title_font',
							'title_font_size',
							'title_font_color',
							'subtitle_font',
							'subtitle_font_size',
							'subtitle_font_color'];

}
