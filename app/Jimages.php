<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Jimages extends Model {

	protected $table = 'js_images';

	protected $fillable = ['domain_id', 'image_order', 'ss_image'];


}
