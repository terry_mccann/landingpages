<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/info', 'IndexController@info');

Route::get('admin', 'AdminController@index');

Route::get('load', 'JSController@defaults');

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin/javascript/sliders/timing', 'JavascriptController@sliderTimingIndex');
    Route::get('admin/javascript/sliders/images', 'JavascriptController@sliderImageIndex');
    Route::get('admin/javascript/sliders/text', 'JavascriptController@sliderTextIndex');
    Route::put('admin/javascript/sliders/text/edit', 'JavascriptController@updateSlideText');
    Route::put('admin/javascript/sliders/reorder', 'JavascriptController@updateSlideOrder');
    Route::put('admin/javascript/sliders/text/reorder/', 'JavascriptController@updateSlideTextOrder');
    Route::delete('admin/javascript/sliders/destroy', 'JavascriptController@deleteSlideImage');
    Route::delete('admin/javascript/sliders/text/destroy', 'JavascriptController@deleteSlideText');
    Route::put('admin/javascript/timing', 'JavascriptController@updateTiming');
    Route::post('admin/javascript/upload', 'JavascriptController@saveImage' );
    Route::post('admin/javascript/upload/slide/text', 'JavascriptController@saveSlideText');
    Route::post('admin/javascript/upload/slide/image', 'JavascriptController@saveSlideImage');
    Route::get('admin/css/fonts', 'CSSController@fontSelectIndex');
    Route::put('admin/css/fonts/update', 'CSSController@fontUpdate');
    Route::get('admin/html/brand', 'HTMLController@brandIndex');
    Route::put('admin/html/nav/changestatus', 'HTMLController@navChangeStatus');
    Route::put('admin/html/brand/navbrand', 'HTMLController@updateBrandText');
    Route::post('admin/html/brand/upload', 'HTMLController@updateBrandImage');
    Route::get('admin/html/sections', 'HTMLController@navIndex');
    Route::get('admin/section/about-us', 'HTMLController@editAbout');
    Route::put('admin/section/about/update', 'HTMLController@updateAbout');
    Route::get('admin/section/locations/add', 'HTMLController@addLocation');
    Route::post('admin/section/locations/save', 'HTMLController@saveLocation');
    Route::get('admin/section/locations', 'HTMLController@editLocations');
    Route::delete('admin/section/location/destroy', 'HTMLController@deleteLocation');
    Route::resource('admin/section/menu', 'MenuController');


});

Route::get('/customjs/', 'JSController@javaScript');
Route::get('/customcss/', 'CSSController@customCSS');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
