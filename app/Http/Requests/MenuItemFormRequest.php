<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class MenuItemFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
		'menu_item_title' => 'required',
		'menu_item_detail' => 'required',
		'menu_item_price' => 'required|numeric'
		];
	}
			

	public function messages()
    {
        return [
        'menu_item_title.required' => 'Menu Item Title is required',
        'menu_item_detail.required' => 'Menu Item Detail is required',
        'menu_item_price.required' => 'Menu Item Price is required',
		'menu_item_price.numeric' => 'Menu Item Price must only contain numbers 0-9, no letters or symbols'
        ];
    }

}
