<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class JSTimingFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
		'preloader_delay' => 'required|numeric',
		'preloader_fade_out' => 'in:fast,slow',
		'ss_image_duration' => 'required|numeric',
		'ss_image_fade' => 'required|numeric',
		'intro_text_pagination_speed' => 'required|numeric',
        'intro_text_autoplay' => 'required|numeric',
        'testimonial_text_pagination_speed' => 'required|numeric',
        'testimonial_text_autoplay'=> 'required|numeric',
		];
	}
			

	public function messages()
    {
        return [
            'preloader_delay.required' => 'Preloader delay is required',
            'ss_image_duration.required' => 'Slide show image duration is required',
			'ss_image_fade.required' => 'Slide show image fade is required',
			'intro_text_pagination_speed.required' => 'Main slider text pagination speed is required',
            'intro_slider_speed.required' => 'Main slider speed is required',
            'intro_slider_autoplay.required' => 'Main slider autoplay is required',
            'testimonial_text_pagination_speed.required' => 'Testimonial text pagination speed is required',
            'testimonial_text_autoplay.required' => 'Testimonial text slider autoplay is required',
            'preloader_delay.numeric' => 'Preloader delay must be a numerical value between 0 and 10000',
            'ss_image_duration.numeric' => 'Slide show image duration must be a numerical value between 0 and 10000',
			'ss_image_fade.numeric' => 'Slide show image fade must be a numerical value between 0 and 10000',
			'intro_text_pagination_speed.numeric' => 'Main slider text pagination speed must be a numerical value between 0 and 10000',
            'intro_slider_autoplay.numeric' => 'Main slider autoplay must be a numerical value between 0 and 10000',
            'testimonial_text_pagination_speed.numeric' => 'Testimonial text pagination speed must be a numerical value between 0 and 10000',
            'testimonial_text_speed.numeric' => 'Testimonial text slider speed must be a numerical value between 0 and 10000',
            'testimonial_text_autoplay.numeric' => 'Testimonial text slider autoplay must be a numerical value between 0 and 10000',
        ];
    }

}
