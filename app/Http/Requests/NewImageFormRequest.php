<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewImageFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
		'image' => 'mimes:jpeg,bmp,png,gif'
		];
	}
			

	public function messages()
    {
        return [
        'image.mimes' => 'File must be of type jpg, bmp, png or gif'
        ];
    }

}
