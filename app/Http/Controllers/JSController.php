<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;
use View;
use App\Javascript;
use App\Jimages;
use App\Helpers;

class JSController extends Controller {

	 public function javaScript()
    {
    	$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

    	$js = \DB::table('javascript')
				->where('uuid', $uuid)
				->get();
    	$images = \DB::table('js_images')
    	->where('uuid', $uuid)
    	->orderBy('image_order')
    	->get();
    	$javascript = View::make('custom.js', compact('js', 'images'));
    	$response = Response::make($javascript);
    	$response->header('Content-Type', 'application/javascript');
			return $response;
    }

}
