<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\MenuItemFormRequest;
use App\Http\Requests\NewMenuItemFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$menu = \DB::table('menu')->
		where('uuid', $uuid)->get();

		return view('admin.menu.index', compact('menu'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.menu.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NewMenuItemFormRequest $request)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$menu = new Menu;
		$menu->uuid = $uuid;
        $menu->menu_item_title = $request->input('menu_item_title');
        $menu->menu_item_detail = $request->input('menu_item_detail');
        $menu->menu_item_price = $request->input('menu_item_price');
        
        $menu->save();

	  		$helpers = new \App\Custom\Helpers;
			$uuid = ($helpers->getUUID());
			$menu = \DB::table('menu')->
			where('uuid', $uuid)->get();

			\Session::flash('message', 'Menu item has been added.');

			return view('admin.menu.index', compact('menu'));

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $menuItem = Menu::find($id);

       return view('admin.menu.edit', compact('menuItem'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(MenuItemFormRequest $request, $id)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$menu = \DB::table('menu')->
		where('uuid', $uuid)->
		where('id', $id)->
		update(['menu_item_title' => $request->input('menu_item_title'),
				'menu_item_detail' => $request->input('menu_item_detail'),
				'menu_item_price' => $request->input('menu_item_price')
			]);

		$menu = \DB::table('menu')->
		where('uuid', $uuid)->get();

		\Session::flash('message', 'Menu Item has been updated.');

           return view('admin.menu.index', compact('menu'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$menu = \DB::table('menu')->
		where('uuid', $uuid)->
		where('id', $id);

		$menu->delete();

		return redirect()->back()->with('message','Your menu item has been deleted.');
	}

}