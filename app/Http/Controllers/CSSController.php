<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\CssFonts;

class CSSController extends Controller {

	public function customCSS()
	{
		$fonts = CssFonts::all();
		$css = \View::make('custom.css', compact('fonts'));
    	$response = \Response::make($css);
    	$response->header('Content-Type', 'text/css');
			return $response;
	}

	public function fontSelectIndex()
	{
		$fonts = CssFonts::all();
		return view('admin.css.fonts.fonts', compact('fonts'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function fontUpdate(Request $request)
	{
		$helpers = new \App\Custom\Helpers;
                	$uuid = ($helpers->getUUID());
	    $selectedFont = $request->input('new_font');

	    switch ($selectedFont) {
	        case "body_font":
	        	\DB::table('css_fonts')
            	->where('uuid', $uuid)
            	->update(['body_font' => $request->input('font'),
            			  'body_font_size' => $request->input('font_size'),
            			  'body_font_color' => $request->input('font_color')
            			   ]);
	            break;
	        case "intro_slider_font":
	            \DB::table('css_fonts')
            	->where('uuid', $uuid)
            	->update(['intro_slider_font' => $request->input('font'),
            			  'intro_slider_font_size' => $request->input('font_size'),
            			  'intro_slider_font_color' => $request->input('font_color')
            			   ]);
	            break;
	        case "title_font":
	            \DB::table('css_fonts')
            	->where('uuid', $uuid)
            	->update(['title_font' => $request->input('font'),
            			  'title_font_size' => $request->input('font_size'),
            			  'title_font_color' => $request->input('font_color')
            			   ]);
	            break;
	        case "subtitle_font":
	            \DB::table('css_fonts')
            	->where('uuid', $uuid)
            	->update(['subtitle_font' => $request->input('font'),
            			  'subtitle_font_size' => $request->input('font_size'),
            			  'subtitle_font_color' => $request->input('font_color')
            			   ]);
	            break;
	        default:
	            \Session::flash('message', 'There was an issue with your font selection!');
        			return redirect()->back();
	    }
        

        \Session::flash('message', 'Your font has been changed!');
        return redirect()->back();
	}


}
