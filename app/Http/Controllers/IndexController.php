<?php namespace App\Http\Controllers;

use App\Jtext;

class IndexController extends Controller {


	/**
	 * Show the application index screen to any user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$slides = \DB::table('js_text')
		->where('uuid', $uuid)
		->orderBy('slide_order', 'asc')
		->get();
		$slideOut = [];

		foreach ($slides as $s) {
			if ($s->slide_type == 'image'){

				$out = '<img src='. $s->image_path . ' />';

			}else{

				$out = '<h1 class="intro-title">' . $s->slide_text . '</h1>';
				
			}
			array_push($slideOut, $out);
			
		}

		 	$navItems = $this->getNavItems($uuid);
		 	$navNames = $this->getNavNames($uuid);
		 	$navBrand = $this->getNavBrand($uuid);
		 	$about = $this->getAbout($uuid);
		 	$locations = $this->getLocations($uuid);
		 	$menu = $this->getMenu($uuid);
		return view('index', compact('slideOut', 'navItems', 'navBrand', 'about', 'locations', 'navNames', 'menu'));
	}

	protected function getNavItems($uuid)
	{
		$navItems = \DB::table('nav_items')
		->where('uuid', $uuid)
		->where('nav_enabled', 1)
		->orderBy('nav_order', 'asc')
		->get();

		return $navItems;
	}

	protected function getNavNames($uuid)
	{
		$navNames = \DB::table('nav_items')
		->where('uuid', $uuid)
		->where('nav_enabled', 1)
		->lists('nav_name');

		return $navNames;
	}

	protected function getNavBrand($uuid)
	{
		$navBrand = \DB::table('nav_brand')
		->where('uuid', $uuid)->get();
			
		return $navBrand;
	}

	protected function getAbout($uuid)
	{
		$about = \DB::table('about')
		->where('uuid', $uuid)->get();

		return $about;
	}

	protected function getLocations($uuid)
	{
		$locations = \DB::table('locations')
		->where('uuid', $uuid)->get();

		return $locations;
	}

	protected function getMenu($uuid)
	{
		$menu = \DB::table('menu')
		->where('uuid', $uuid)->get();

		return $menu;
	}


}
