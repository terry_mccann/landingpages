<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\NewImageFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Locations;

class HTMLController extends Controller {

	
	public function brandIndex()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$navBrand = \DB::table('nav_brand')->
		where('uuid', $uuid)->get();

		return view('admin.html.brand.index', compact('navBrand'));
	}

	public function navIndex()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$navItems = \DB::table('nav_items')->
		select('nav_name', 'nav_enabled', 'id')->
		where('uuid', $uuid)->
		whereIn('nav_name', ['About Us', 'Locations', 'Menu'])->get();

		return view('admin.html.sections.index', compact('navItems'));
	}

	public function navChangeStatus(Request $request)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$id = $request->input('id');

		\DB::table('nav_items')->
		where('id', $id)->
		where('uuid', $uuid)->
		update(['nav_enabled' => $request->input('changeStatus')]);

		return redirect()->back()->with('message','Your menu item status has been changed');
	}

	public function updateBrandImage(NewImageFormRequest $request)
	{      
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		      $file = $request->file('image');
		      $filename = $file->getClientOriginalName();

		      $original = \Image::make($file)->save(public_path('brandimages/'. $filename));
		      
		      $pathOriginal = '/brandimages/'. $filename;

		      \DB::table('nav_brand')
		      	->where('uuid', $uuid)
		      	->update(['nav_brand_type' => 'image',
		      			  'nav_image' => $pathOriginal]);

		      return redirect()->back()->with('message','Your new nav brand image has been uploaded.');
		        
	}

	public function updateBrandText(Request $request)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$id = $request->input('id');
		$text = \DB::table('nav_brand')
					->where('id', $id)
					->update(['nav_text' => $request->input('nav_text')]);

		return redirect()->back()->with('message','Brand text has been updated.');
	}

	public function editAbout() 
	{
		$about = \DB::table('about')->get();

		return view('admin.html.sections.about_edit', compact('about'));
	}

	public function updateAbout(Request $request) 
	{
		$id = $request->input('id');
		$about = \DB::table('about')
					->where('id', $id)
					->update(['about_title' => $request->input('about_title'),
							  'about_subtitle' => $request->input('about_subtitle'),
							  'about_text' => $request->input('about_text')]);

		return redirect()->back()->with('message','About section has been updated.');
	}

	public function addLocation()
	{
		return view('admin.html.sections.locations_add');
	}

	public function saveLocation(Request $request)
	{
		$location = new Locations;
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$location->uuid = $uuid;
		$location->location_header = $request->input('location_header');
		$location->location_name = $request->input('location_name');
		$location->location_address = $request->input('location_address');
		$location->location_city = $request->input('location_city');
		$location->location_state = $request->input('location_state');
		$location->location_zip = $request->input('location_zip');
		$location->location_phone = $request->input('location_phone');

		$location->save();
		return redirect()->back()->with('message','New location has been added.');

	}

	public function editLocations() 
	{
		$locations = \DB::table('locations')->get();

		return view('admin.html.sections.locations_edit', compact('locations'));
	}

	public function deleteLocation(Request $request) 
	{
		$id = $request->input('id');

		$location = Locations::find($id);
		$location->delete();

		return redirect()->back()->with('message','Location has been deleted.');
	}
	
}