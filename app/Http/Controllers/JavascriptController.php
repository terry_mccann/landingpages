<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\JSTimingFormRequest;
use App\Http\Requests\NewImageFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Javascript;
use App\Jimages;
use App\Jtext;
use Carbon\Carbon;
use App\Helpers;

class JavascriptController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function sliderTimingIndex()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$js = \DB::table('javascript')
				->where('uuid', $uuid)
				->get();

		return view('admin.js.timing.index', compact('js'));
	}

	public function sliderImageIndex()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$js = \DB::table('javascript')
				->where('uuid', $uuid)
				->get();

		$images = \DB::table('js_images')
		->where('uuid', $uuid)
		->orderBy('image_order', 'asc')->get();

		return view('admin.js.images.index', compact('images', 'js'));
	}

	public function sliderTextIndex()
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$text = \DB::table('js_text')
				->where('uuid', $uuid)
				->orderBy('slide_order', 'asc')
				->get();

		return view('admin.js.text.index', compact('text'));
	}

	
	public function updateTiming(JSTimingFormRequest $request)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());
		$domain = $request->input('domain');
	    $timing = \DB::table('javascript')
					->where('uuid', $uuid)
					->update(['preloader_delay' => $request->input('preloader_delay'),
							  'preloader_fade_out' => $request->input('preloader_fade_out'),
							  'ss_image_duration' => $request->input('ss_image_duration'),
							  'ss_image_fade' => $request->input('ss_image_fade'),
							  'intro_text_pagination_speed' => $request->input('intro_text_pagination_speed'),
							  'intro_text_autoplay' => $request->input('intro_text_autoplay'),
							  'testimonial_text_pagination_speed' => $request->input('testimonial_text_pagination_speed'),
							  'testimonial_text_autoplay' => $request->input('testimonial_text_autoplay')
								]);
        

        \Session::flash('message', 'Timing changed!');
        return redirect()->back();

	}

	public function saveImage(NewImageFormRequest $request)
	{      
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$image = new Jimages;

		      $file = $request->file('image');
		      $filename = $file->getClientOriginalName();

		      $original = \Image::make($file)->save(public_path('jsimages/'. $filename));
		      
		      $pathOriginal = '/jsimages/'. $filename;

		      $image->uuid = $uuid;
		      $image->ss_image = $pathOriginal;
		      $image->save();

		      return redirect()->back()->with('message','Your new slide images has been uploaded.');
		        
	}

	public function saveSlideImage(NewImageFormRequest $request)
	{      
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$image = new Jtext;

		      $file = $request->file('image');
		      $filename = $file->getClientOriginalName();

		      $original = \Image::make($file)->save(public_path('slideimages/'. $filename));
		      
		      $pathOriginal = '/slideimages/'. $filename;

		      $image->uuid = $uuid;
		      $image->image_path = $pathOriginal;
		      $image->slide_type = 'image';
		      $image->save();

		      return redirect()->back()->with('message','Your new slide image has been uploaded.');
		        
	}

	public function saveSlideText(Request $request)
	{
		$helpers = new \App\Custom\Helpers;
		$uuid = ($helpers->getUUID());

		$text = new Jtext;

		$text->uuid = $uuid;
		$text->slide_text = $request->input('slide_text');
		$text->slide_type = 'text';
		$text->save();

		return redirect()->back()->with('message','Your new slide has been saved.');
	}

	public function updateSlideOrder(Request $request)
	{
		$id = $request->input('id');
		$reorder = \DB::table('js_images')
					->where('id', $id)
					->update(['image_order' => $request->input('reorder')]);

		return redirect()->back()->with('message','Slide order has been updated.');
	}

	public function updateSlideTextOrder(Request $request)
	{
		$id = $request->input('id');
		\DB::table('js_text')
					->where('id', $id)
					->update(['slide_order' => $request->input('reorder')]);

		return redirect()->back()->with('message','Slide order has been updated.');
	}

	public function updateSlideText(Request $request)
	{
		$id = $request->input('id');
		\DB::table('js_text')
			->where('id', $id)
			->update(['slide_text' => $request->input('slide_text')]);

		return redirect()->back()->with('message','Slide text has been updated.');
	}

	public function deleteSlideImage(Request $request)
	{
		$id = $request->input('id');
		$slide = Jimages::find($id);
		$slide->delete();

		return redirect()->back()->with('message','Slide has been deleted.');
	}

	public function deleteSlideText(Request $request)
	{
		$id = $request->input('id');
		$slide = Jtext::find($id);
		$slide->delete();

		return redirect()->back()->with('message','Slide has been deleted.');
	}


}
