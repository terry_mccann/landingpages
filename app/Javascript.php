<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Javascript extends Model {

	protected $table = 'javascript';

	protected $fillable = ['domain'];

}
