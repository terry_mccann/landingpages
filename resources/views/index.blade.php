<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!--FACEBOOK OPEN GRAPH TAGS -->
    <meta property="og:type"                				content="restaurant.restaurant" /> 
	<meta property="og:url"                                 content="http://52.91.203.253/" /> 
	<meta property="og:title"                               content="Pizza Snapp" /> 
	<meta property="og:image"                               content="http://52.91.203.253/assets/images/Pizza3.jpg" /> 
	<meta property="og:description"                         content="Authentic Pizza Di Napoli Wood Fired Pizza" /> 
	<meta property="restaurant:contact_info:street_address" content="120 East Ave." /> 
	<meta property="restaurant:contact_info:locality"       content="Rochester" /> 
	<meta property="restaurant:contact_info:region"         content="New York" /> 
	<meta property="restaurant:contact_info:postal_code"    content="14604" /> 
	<meta property="restaurant:contact_info:country_name"   content="United States" /> 
	<meta property="restaurant:contact_info:email"          content="terry@ordersnapp.com" /> 
	<meta property="restaurant:contact_info:phone_number"   content="1-888-402-6863" /> 
	<meta property="restaurant:contact_info:website"        content="http://www.ordersnapp.com" /> 
	<meta property="place:location:latitude"                content="43.16103000" /> 
	<meta property="place:location:longitude"               content="-77.61092400" /> 

    <!--TWITTER CARD-->
    <meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@woodfiredpizza" />
	<meta name="twitter:creator" content="@OrderSnapp" />
	<meta name="twitter:title" content="Authentic Italian Wood Fired Pizza" />
	<meta name="twitter:description" content="The Wood Fired Pizza Shop crafts every pizza in the traditional Old-World Italian way." />
	<meta name="twitter:image" content="http://52.91.203.253/assets/images/twitter-card.jpg" />

	<title>Demo Landing Page</title>

	<!-- CSS -->
	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	<!-- Elegant icons CSS -->
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" media="screen">

	<!--[if lte IE 7]>
		<script src="assets/js/icons-lte-ie7.js"></script>
	<![endif]-->

	<!-- Magnific-popup lightbox -->
	<link href="assets/css/magnific-popup.css" rel="stylesheet">

	<!-- Owl Carousel -->
	<link href="assets/css/owl.theme.css" rel="stylesheet">
	<link href="assets/css/owl.carousel.css" rel="stylesheet">

	<!-- Animate css -->
	<link href="assets/css/animate.css" rel="stylesheet">

	<!-- Time and Date piker CSS -->
	<link href="assets/css/jquery.timepicker.css" rel="stylesheet" media="screen">
	<link href="assets/css/datepicker.css" rel="stylesheet" media="screen">

	<!-- Custom styles CSS -->
	<link href="{{ url('/customcss') }}" rel="stylesheet" media="screen">
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Restaurant",
	  "@id": "http://pizzasnapp.com",
	  "name": "Pizza-Snapp",
	  "address": {
	    "@type": "PostalAddress",
	    "streetAddress": "120 East Ave, Suite 325",
	    "addressLocality": "Rochester",
	    "addressRegion": "NY",
	    "postalCode": "14604",
	    "addressCountry": "US"
	  },
	  "geo": {
	    "@type": "GeoCoordinates",
	    "latitude": 43.15694900,
	    "longitude": -77.60122200
	  },
	  "url": "http://pizzasnapp.com/#locations",
	  "telephone": "1-888-402-6863",
	  "openingHoursSpecification": [
	    {
	      "@type": "OpeningHoursSpecification",
	      "dayOfWeek": [
	        "Monday",
	        "Tuesday"
	      ],
	      "opens": "11:30",
	      "closes": "22:00"
	    },
	    {
	      "@type": "OpeningHoursSpecification",
	      "dayOfWeek": [
	        "Wednesday",
	        "Thursday",
	        "Friday"
	      ],
	      "opens": "11:30",
	      "closes": "23:00"
	    },
	    {
	      "@type": "OpeningHoursSpecification",
	      "dayOfWeek": "Saturday",
	      "opens": "16:00",
	      "closes": "23:00"
	    },
	    {
	      "@type": "OpeningHoursSpecification",
	      "dayOfWeek": "Sunday",
	      "opens": "16:00",
	      "closes": "22:00"
	    }
	  ],
	  "menu": "http://www.pizzasnapp.com/#menu",
	  "acceptsReservations": "True"
	}
	</script>
	
</head>
<body>
	<!-- Preloader -->

	<div id="preloader">
		<div id="status">
			<div class="status-mes"></div>
		</div>
	</div>

	<!-- Navigation start -->

	<nav class="navbar navbar-custom navbar-transparent navbar-fixed-top" role="navigation">

		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				@if($navBrand[0]->nav_brand_type == 'image')
				<a class="navbar-brand" href="index.html"><img src="{{ $navBrand[0]->nav_image }}"></a>
				@else
				<a class="navbar-brand" href="index.html">{{ $navBrand[0]->nav_text }}</a>
				@endif
			</div>

			<div class="collapse navbar-collapse" id="custom-collapse">
				<ul class="nav navbar-nav navbar-right">
					@foreach($navItems as $ni)
					<li class="page-scroll"><a href="{{ $ni->nav_path }}">{{ $ni->nav_name }}</a></li>
					@endforeach
				</ul>
			</div>

		</div><!-- .container -->

	</nav>

	<!-- Navigation end -->

	<!-- Home start -->

	<section id="home" class="intro-module slideshow heightfull bg-dark-alfa-30">

		<div class="intro">

			<div class="intro-slider">
				@foreach($slideOut as $s)
				<div class="owl-item">
					{!! $s !!}
				</div>
				@endforeach

			</div><!-- .intro-slider -->

		</div><!-- .intro -->

		<div class="mouse-icon">
			<div class="wheel"></div>
		</div>

	</section>

	@if(in_array('About Us', $navNames))
	 @include('sections.about')
	@endif
	
	@if(in_array('Locations', $navNames))
	 @include('sections.locations')
	@endif

	@if(in_array('Menu', $navNames))
	 @include('sections.menu')
	@endif

	<!-- Callout section end -->

	<!-- Map start -->

	<section id="map-section" class="heightfull">

		<div id="map"></div>

		<div class="mouse-icon">
			<div class="wheel"></div>
		</div>

	</section>

	<!-- Map end -->

	<!-- Contact start -->

	<section id='contact' class="module">
		<div class="container">

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="module-header">
						<p class="module-title">Contact us</p>
						<p class="module-subtitle">Drop us a line</p>
					</div>
				</div>
			</div><!-- .row -->

			<div class="row">

				<div class="col-md-6 col-sm-offset-3">
					<form id="contact-form" role="form" novalidate>

						<div class="row">

							<div class="col-sm-6 form-group">
								<label class="sr-only" for="cname">Name</label>
								<input type="text" id="cname" class="form-control" name="cname" placeholder="Name" required data-validation-required-message="Please enter your name.">
								<p class="help-block text-danger"></p>
							</div>

							<div class="col-sm-6 form-group">
								<label class="sr-only" for="cemail">Your Email</label>
								<input type="email" id="cemail" name="cemail" class="form-control" placeholder="Your Email" required data-validation-required-message="Please enter your email address.">
								<p class="help-block text-danger"></p>
							</div>

						</div><!-- .row -->

						<div class="row">
							<div class="col-sm-12 form-group">
								<textarea class="form-control" id="cmessage" name="cmessage" rows="4" placeholder="Message" required data-validation-required-message="Please enter your message."></textarea>
								<p class="help-block text-danger"></p>
							</div>
						</div><!-- .row -->

						<div class="row">
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-custom-1">Submit</button>
							</div>
						</div><!-- .row -->

					</form>
					<!-- Ajax response -->
					<div id="contact-response" class="ajax-response"></div>
				</div><!-- .col -->

			</div><!-- .row -->

		</div><!-- .container -->
	</section>

	<!-- Contact end -->

	<!-- Footer start -->

	<footer id="footer">
		<div class="container">

			<div class="row">
				<div class="col-sm-12">
					<p class="copyright">
						© 2016 <a href="http://www.ordersnapp.com">OrderSnapp.com</a>, All Rights Reserved.
					</p>
				</div>
			</div><!-- .row -->

		</div><!-- .container -->
	</footer>

	<!-- Footer end -->

	<!-- Scroll-up -->

	<div class="scroll-up">
		<a href="#totop"><i class="fa fa-angle-double-up"></i></a>
	</div>

	<!-- Javascript files -->
	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>

	<!-- Use for slideshow background -->
	<script src="assets/js/jquery.backstretch.min.js"></script>

	<!-- Google Maps -->
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyAsfKVKuIKNbhBYE9hFE6UMSZxrVM7dVus"></script>
	<script src="assets/js/gmaps.js"></script>

	<!-- Other -->
	<script src="assets/js/jquery.matchHeight-min.js"></script>
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/wow.min.js"></script>

	<!-- Custom scripts -->
	<script src="{{ url('/customjs') }}"></script>
</body>
</html>