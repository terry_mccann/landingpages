@extends('main')

@section('content')
     <?php
		$b_font = str_replace('+', ' ', $fonts[0]->body_font);
		$i_font = str_replace('+', ' ', $fonts[0]->intro_slider_font);
		$t_font = str_replace('+', ' ', $fonts[0]->title_font);
		$s_font = str_replace('+', ' ', $fonts[0]->subtitle_font);
	?>

<link href='https://fonts.googleapis.com/css?family={{$fonts[0]->body_font}}:300,400,700,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family={{$fonts[0]->intro_slider_font}}:300,400,700,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family={{$fonts[0]->title_font}}:300,400,700,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family={{$fonts[0]->subtitle_font}}:300,400,700,400italic' rel='stylesheet' type='text/css'>
<link href="/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<br>
<br>
@if (Session::has('message'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
{{ Session::get('message') }}
</div>
@endif
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Website Font Selector</div>
				<div class="panel-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label">Change This Font</label>
							<div class="col-md-6">
								<select id="fontSelected" class="form-control">
									<option value="body_font">Body Font</option>
									<option value="intro_slider_font">Intro Slider Font</option>
									<option value="title_font">Title Font</option>
							        <option value="subtitle_font">Subtitle</option>
							    </select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Select Font</label>
							<div class="col-md-6">
								<select id="select_fontfamily" style="width: 230px;"></select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Font Size</label>
							<div class="col-md-3">
								<select id="size" class="form-control">
									<option value="14">14</option>
									<option value="16">16</option>
									<option value="18">18</option>
							        <option value="20">20</option>
							        <option value="22">22</option>
							        <option value="24">24</option>
							        <option value="26">26</option>
							        <option value="28">28</option>
							        <option value="32">32</option>
							        <option value="36">36</option>
							        <option value="40">40</option>
							        <option value="44">44</option>
							        <option value="48">48</option>
							        <option value="60">60</option>
							        <option value="72">72</option>
							    </select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Font Color</label>
							<div class="col-md-6">
								<input type="text" id="color" class="form-control"/>
							</div>
						</div>

					    <div class="form-group">
					    	<label class="col-md-3 control-label">Custom Preview</label>
					    	<div class="col-md-6">
					    		<input type="text" id="text" class="form-control">
					    		<span id="helpBlock" class="help-block">Type anything in here for a preview. Text will not be saved.</span>
					    	</div>
					    </div>
					   
					</form>
				    <form class="form-horizontal" method="post" action="{{url('admin/css/fonts/update/')}}">
				    	<input type="hidden" name="_method" value="put">
				    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				    	<input type="hidden" id="new-font" name="new_font" value="body_font" />
				    	<input type="hidden" id="font" name="font" value="arial" />
				    	<input type="hidden" id="px" name="font_size" value="14" />
				    	<input type="hidden" id="font-color" name="font_color" />
				    	<div class="form-group">
							<div class="col-md-6 col-md-offset-2">
								<button type="submit" class="btn btn-primary">
									Save
								</button>
							</div>
						</div>
				    </form>
				    <hr>
				    <div class="well well-sm well-dark">
					<p id="preview" style="font-size: 14px;">This is just a preview of what your new font will look like.</p>
					</div>
				</div>
		</div>
    </div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Current Fonts</div>
				<div class="panel-body">
					<div class="row">
					  		<div class="col-md-12"><span style="font-family:{{$i_font}}; font-size:{{$fonts[0]->intro_slider_font_size}}px; font-weight:700;">Current Intro Slider/Section Header Font <em>({{$i_font}})</em></span></div>
					</div>
					<hr>
					<div class="row">
					  		<div class="col-md-12"><span style="font-family:{{$b_font}}; font-size:{{$fonts[0]->body_font_size}}px; color:{{$fonts[0]->body_font_color}}; font-weight: bold;" >Current Body Font <em>({{$b_font}})</em></span></div>
					</div>
					<hr>
					<div class="row">
					  		<div class="col-md-12"><span style="font-family:{{$t_font}}; font-size:32px;" >Current Title Font <em>({{$t_font}})</em></span></div>
					</div>
					<hr>
					<div class="row">
					  		<div class="col-md-12"><span style="font-family:{{$s_font}}; font-size:{{$fonts[0]->subtitle_font_size}}px; color:{{$fonts[0]->subtitle_font_color}}" >CURRENT SUBTITLE FONT <em>({{$s_font}})</em></span></div>
					</div>
				</div>
		</div>
	</div>
</div>
<script src="/js/bootstrap-colorpicker.js"></script>

<script type="text/javascript">
$(document).ready(function() {

    $( "#select_fontfamily" ).higooglefonts({           
        selectedCallback:function(e){
            //console.log(e);
        },
        loadedCallback:function(e){
            //console.log(e);
            $("p").css("font-family", e);
            var url = e.split(' ').join('+');
            $("input:hidden#font").val(url);
        }           
    });

    	$("#size").change(function() {
    	    $('p').css("font-size", $(this).val() + "px");
    	    $("input:hidden#px").val($(this).val());
    	});

    	$(function(){
    	  $('#text').keyup(function(){
    	     $('#preview').text($(this).val());
    	  });
    	});

    	$(function(){
    	        $('input:text#color').colorpicker({
			      format: 'rgba',
			      horizontal: true
			    }).on('changeColor', function(ev) {
			    	   newColor = ev.color.toRGB();
			    	   r = newColor.r;
			    	   g = newColor.g;
			    	   b = newColor.b;
			    	   a = newColor.a;
						$('input:hidden#font-color').val('rgba('+ r +','+ g +','+ b +','+ a +')');
						$('p').css("color", 'rgba('+ r +','+ g +','+ b +','+ a +')');
					});
    	 });

    	$('#fontSelected').change(function(){
    	   $('input:hidden#new-font').val($(this).val());
    	});


  });

</script>

@endsection