@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit {{ $menuItem->menu_item_title }}
				</div>
				<div class="panel-body">
				<form class="form-horizontal" role="form" action=" {{ url('admin/section/menu/' . $menuItem->id) }}" method="post">
					<input type="hidden" name="_method" value="put">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<label class="col-md-2 control-label"><strong>Item Name</strong></label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="menu_item_title" value="{{ $menuItem->menu_item_title }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label"><strong>Item Detail</strong></label>
						<div class="col-md-8">
							<textarea cols="4" class="form-control" name="menu_item_detail">{{ $menuItem->menu_item_detail }}</textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label"><strong>Item Price</strong></label>
						<div class="col-md-4">
							<div class="input-group">
  								<span class="input-group-addon">$</span>
									<input type="text" class="form-control" name="menu_item_price" value="{{$menuItem->menu_item_price}}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-2">
							<button type="submit" class="btn btn-primary btn-sm">
								Save
							</button>
						</div>
					</div>
				</form>
			</div>
			</div>
	    </div>
	</div>
@endsection