@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Menu Items
					<div class="pull-right">
						<a class="btn btn-success btn-xs" href="{{ url('admin/section/menu/create') }}">Add Menu Item</a>
					</div>
				</div>
				<table class="table table-striped table-bordered">
				    <thead>
				        <tr>
				            <td><strong>Item Name</strong></td>
				            <td><strong>Item Description</strong></td>
				            <td><strong>Item Price</strong></td>
				            <td><strong>Actions</strong></td>
				        </tr>
				    </thead>
				    <tbody>
				    @foreach($menu as $key => $value)
				        <tr>
				            <td>{{ $value->menu_item_title }}</td>
				            <td>{{ $value->menu_item_detail }}</td>
				            <td>{{ $value->menu_item_price }}</td>
				            <td>
				                <a class="btn btn-xs btn-info" href="{{ url('admin/section/menu/' . $value->id . '/edit') }}">Edit Item</a>
				                <form action="{{ url('admin/section/menu/' . $value->id) }}" method="post" class="pull-right">
				                	<input type="hidden" name="_method" value="delete">
		    						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    						<button class="btn btn-danger btn-xs" type="submit">
			    							Delete Item
			    						</button>
				                </form>
				            </td>
				        </tr>
				    @endforeach
				    </tbody>
				</table>
			</div>
	    </div>
	</div>
@endsection