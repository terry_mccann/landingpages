@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
			<div class="panel-heading">Main Slider Images</div>
				<div class="panel-body">
					@if(isset($success))
				       <div class="alert alert-success alert-dismissible" role="alert"> 
				       	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				       	{{$success}} 
				       </div>
				   	@endif
				    	
				    	<table class="table">
				    		<thead>
				    			<tr><th>Image</th><th>Current Order</th><th>Reorder Slides</th><th>Delete</th></tr>
				    		</thead>
				    		@foreach($images as $i)
				    		<tr>
				    			<td><img src="{{$i->ss_image}}" class="img-responsive thumbnail" style="max-height: 80px;"></td>
				    			<td>{{$i->image_order}}</td>
				    			<td>
				    				<form class="form-inline" role="form" method="post" action="{{ url('admin/javascript/sliders/reorder/') }}">
				    					<input type="hidden" name="_method" value="put" />
				    					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				    					<input type="hidden" name="id" value="{{ $i->id }}" />
										<select class="form-control input-sm" name="reorder">
										<?php $n = 1;?>
										@foreach($images as $z)
										  <option value="{{$n}}">{{$n}}</option>
										  <?php $n++;?>
										@endforeach
										</select>
										<button type="submit" class="btn btn-primary btn-xs">Update</button>
									</form>
				    			</td>
				    			<td>
				    				<form class="form-inline" role="form" method="post" action="{{ url('admin/javascript/sliders/destroy/') }}">
				    					<input type="hidden" name="_method" value="delete">
				    					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				    					<input type="hidden" name="id" value="{{ $i->id }}" />
				    					<input class="btn btn-danger btn-xs" type="submit" value="Delete">
				    				</form>
				    			</td>
				    		</tr>
				    		@endforeach
				    	</table>
				    	<hr>
				    <div class="well">
				   	<form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('admin/javascript/upload') }}">
				   		<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Choose An Image</label>
							<div class="col-md-8">
								<input type="file" name="image">
								<span id="helpBlock" class="help-block">Optimal size is 1920 x 1280</span>
							</div>
						</div>

				       	<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary btn-xs">
									Save
								</button>
							</div>
						</div>
				    </form>
				</div>
				</div>
			</div>
	    </div>
	</div>
@endsection