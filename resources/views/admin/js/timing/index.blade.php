@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
			<div class="panel panel-default">
				<div class="panel-heading">Javascript Timing<span class="pull-right"><small>1000 = 1 second</small></span></div>
					<div class="panel-body">
						<form class="form-horizontal" role="form" method="post" action="{{ url('admin/javascript/timing') }}">
							<input type="hidden" name="_method" value="put">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="form-group">
								<label class="col-md-4 control-label"><strong>Preloader Delay</strong></label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="preloader_delay" value="{{ $js[0]->preloader_delay }}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 350</small></span><br>Preloader delay is the duration the preloader is visible before the site loads.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label"><strong>Preloader Fade Out</strong></label>
								<div class="col-md-6">
									<select class="form-control">
									  <option name="preloader_fade_out" value="slow">slow</option>
									  <option name="preloader_fade_out" value="fast">fast</option>
									</select>
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = slow</small></span><br>Preloader fade out is the transition effect from preloader to main site.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Slider Image Duration</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="ss_image_duration" value="{{$js[0]->ss_image_duration}}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 3000</small></span><br>The time slides are stationary before transitioning to next slide.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Slider Image Fade</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="ss_image_fade" value="{{$js[0]->ss_image_fade}}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 600</small></span><br>Transition time between images.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Slider Text Transition Speed</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="intro_text_pagination_speed" value="{{$js[0]->intro_text_pagination_speed}}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 600</small></span><br>Transition time between text.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Slider Text Duration</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="intro_text_autoplay" value="{{$js[0]->intro_text_autoplay}}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 3000</small></span><br>The amount of time your text is stationary on the screen.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Testimonial Transition Speed</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="testimonial_text_pagination_speed" value="{{$js[0]->testimonial_text_pagination_speed}}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 600</small></span><br>Transition time between testimonials.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Testimonial Duration</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="testimonial_text_autoplay" value="{{$js[0]->testimonial_text_autoplay}}">
									<span id="helpBlock" class="help-block"><span class="text-danger"><small>Default = 3000</small></span><br>The amount of time your testimonial is stationary on the screen.</span>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="submit" class="btn btn-primary">
										Save
									</button>
								</div>
							</div>
						</form>
					</div>
			</div>
	    </div>
	</div>
@endsection
