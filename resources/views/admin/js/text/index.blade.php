@extends('main')

@section('content')
<br>
<br>
@if (Session::has('message'))
<div class="alert alert-info alert-dismissable">
	{{ Session::get('message') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
@if (count($errors) > 0)
	<div class="alert alert-danger alert-dismissable">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			<div class="panel-heading">Main Slider Text</div>
		    	<table class="table">
		    		<thead style="text-align:center">
		    			<tr><th>Image/Text</th><th>Current Order</th><th>Reorder Slides</th><th>Delete</th></tr>
		    		</thead>
		    		@foreach($text as $t)
		    		<tr>
		    			@if($t->slide_type == 'image')
		    			
		    			<td><div class="well well-sm well-dark"><img src="{{ $t->image_path }}" class="img-responsive" style="max-height: 75px;" /></div></td>

		    			@else
		    			<td>
		    				<form class="form-inline" role="form" method="post" action="{{ url('admin/javascript/sliders/text/edit')}}">
			    				<input type="hidden" name="_method" value="put" />
			    				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			    				<input type="hidden" name="id" value="{{ $t->id }}" />
			    				<div class="col-md-8">
			    				<input style="width:100%;" type="text" name="slide_text" value="{{ $t->slide_text }}" class="form-control"><br>
			    				</div>
			    				<button type="submit" class="btn btn-success btn-xs">Edit Slide Text</button>
		    				</form>
		    			</td>
		    			@endif
		    			<td align="center">{{$t->slide_order}}</td>
		    			<td>
		    				<form class="form-inline" role="form" method="post" action="{{ url('admin/javascript/sliders/text/reorder/') }}">
		    					<input type="hidden" name="_method" value="put" />
		    					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		    					<input type="hidden" name="id" value="{{ $t->id }}" />
								<select class="form-control input-sm" name="reorder">
								<?php $n = 1;?>
								@foreach($text as $p)
								  <option value="{{$n}}">{{$n}}</option>
								  <?php $n++;?>
								@endforeach
								</select>
								<button type="submit" class="btn btn-primary btn-xs">Update</button>
							</form>
		    			</td>
		    			<td>
		    				<form class="form-inline" role="form" method="post" action="{{ url('admin/javascript/sliders/text/destroy/') }}">
		    					<input type="hidden" name="_method" value="delete">
		    					<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    					<input type="hidden" name="id" value="{{ $t->id }}" />
		    					<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
		    				</form>
		    			</td>
		    		</tr>
		    		@endforeach
		    	</table>
				    	<hr>
				    	<div class="col-md-6">
				    		<h3>Add new text slide</h3>
				    		<div class="well">
				    			<form class="form-horizontal" method="post" action="{{ url('admin/javascript/upload/slide/text') }}">
				    				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				    				<div class="form-group">
				    					<label class="col-md-3 control-label"><strong>Slide Text</strong></label>
				    					<div class="col-md-8">
				    						<input type="text" class="form-control" name="slide_text">
				    					</div>
				    				</div>

				    				<div class="form-group">
				    					<div class="col-md-6 col-md-offset-4">
				    						<button type="submit" class="btn btn-primary">
				    							Save
				    						</button>
				    					</div>
				    				</div>
				    			</form>
				    		</div>
				    	</div>
				    	<div class="col-md-6">
				    		<h3>Add new image slide</h3>
						    <div class="well">
						   	<form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('admin/javascript/upload/slide/image') }}">
						   		<input type="hidden" name="_token" value="{{ csrf_token() }}">

								<div class="form-group">
									<label class="col-md-4 control-label">Choose An Image</label>
									<div class="col-md-8">
										<input type="file" name="image">
										<span id="helpBlock" class="help-block">Optimal size is 1920 x 1280</span>
									</div>
								</div>

						       	<div class="form-group">
									<div class="col-md-6 col-md-offset-4">
										<button type="submit" class="btn btn-primary">
											Save
										</button>
									</div>
								</div>
						    </form>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
@endsection