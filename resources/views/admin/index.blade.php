@extends('main')

@section('content')
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h5>Welcome {{Auth::user()->name}}.</h5>
		</div>
	</div>
</div>
@endsection