@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Navbar Brand</div>
					<div class="panel-body">
						<div class="well well-sm well-dark">
						@if(isset($navBrand[0]->nav_image))
							<img src="{{ $navBrand[0]->nav_image }}" class="img-responsive" />
						</div>
							<hr>
						@endif
						<form class="form-horizontal" role="form" method="post" action="{{ url('admin/html/brand/navbrand') }}">
							<input type="hidden" name="_method" value="put">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id" value="{{ $navBrand[0]->id }}"

							<div class="form-group">
								<label class="col-md-3 control-label"><strong>Brand Text</strong></label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="nav_text" value="{{ $navBrand[0]->nav_text }}">
									<br>
									<button type="submit" class="btn btn-primary btn-xs">Change</button>
								</div>
							</div>
						</form>
						<hr>
			    		<h4>Add a Nav Brand Logo</h4>
					    <div class="well">
						   	<form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('admin/html/brand/upload') }}">
						   		<input type="hidden" name="_token" value="{{ csrf_token() }}">

								<div class="form-group">
									<label class="col-md-4 control-label">Choose An Image</label>
									<div class="col-md-8">
										<input type="file" name="image">
										<span id="helpBlock" class="help-block">Optimal size for a brand logo is 200px x 80px</span>
									</div>
								</div>

						       	<div class="form-group">
									<div class="col-md-6 col-md-offset-4">
										<button type="submit" class="btn btn-primary btn-xs">
											Save
										</button>
									</div>
								</div>
						    </form>
						</div>
					</div>
			</div>
	    </div>
	</div>
@endsection
