@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Navbar Items</div>
					<div class="panel-body">
						<table class="table">
				    		<thead style="text-align:center">
				    			<tr><th>Item Name</th><th>Status</th><th>Enable/Disable</th></tr>
				    		</thead>
				    		@foreach($navItems as $item)
				    		<tr>
				    			<td>
				    				{{ $item->nav_name }}
				    			</td>
				    			<td align="center">
				    				@if($item->nav_enabled == 1)
				    					Enabled
				    				@else
				    					Disabled
				    				@endif
				    			</td>
				    			<td>
				    				<form class="form-inline" role="form" method="post" action="{{ url('admin/html/nav/changestatus') }}">
				    					<input type="hidden" name="_method" value="put" />
				    					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				    					<input type="hidden" name="id" value="{{ $item->id }}" />
										<select class="form-control input-sm" name="changeStatus">
										  <option value="1">Enable</option>
										  <option value="0">Disable</option>
										</select>
										<button type="submit" class="btn btn-primary btn-xs">Update</button>
									</form>
				    			</td>
				    		</tr>
				    		@endforeach
				    	</table>
					</div>
			</div>
	    </div>
	</div>
@endsection
