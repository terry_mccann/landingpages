@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					Add Locations
				</div>
					<div class="panel-body">
						<br>
						<form class="form-horizontal" role="form" method="post" action="{{ url('admin/section/locations/save') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>Location Header</strong></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="location_header">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>Location Name</strong></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="location_name">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>Address</strong></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="location_address">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>City</strong></label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="location_city">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>State</strong></label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="location_state">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>Zip</strong></label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="location_zip">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>Phone</strong></label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="location_phone">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-2">
									<button type="submit" class="btn btn-primary btn-sm">
										Add Location
									</button>
								</div>
							</div>
						</form>
					</div>
			</div>
	    </div>
	</div>
@endsection
