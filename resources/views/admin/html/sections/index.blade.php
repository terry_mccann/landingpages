@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">Enable/Disable Sections</div>
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr><th>Item Name</th><th>Status</th><th>Enable/Disable</th><th>Edit Section</th></tr>
							</thead>
							@foreach($navItems as $item)
							<?php $name = str_slug($item->nav_name);?>
							<tr>
								<td>
									{{ $item->nav_name }}
								</td>
								<td>
									{{$item->nav_enabled == 1 ? 'Enabled' : 'Disabled'}}
								</td>
								<td>
									<form class="form-inline" role="form" method="post" action="{{ url('admin/html/nav/changestatus') }}">
										<input type="hidden" name="_method" value="put" />
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="id" value="{{ $item->id }}" />
										<select class="form-control input-sm" name="changeStatus">
										  <option value="1">Enable</option>
										  <option value="0">Disable</option>
										</select>
										<button type="submit" class="btn btn-primary btn-xs">Update</button>
									</form>
								</td>
								<td>
									<a href="{{ url('admin/section/').'/' .$name }}" class="btn btn-success btn-xs" role="button">Edit {{ $item->nav_name }}</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
			</div>
	    </div>
	</div>
@endsection
