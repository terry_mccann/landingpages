@extends('main')

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-6">
		@if (Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ Session::get('message') }}
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger alert-dismissable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
</div>
	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading">Edit About Section Text</div>
					<div class="panel-body">
						<br>
						<form class="form-horizontal" role="form" method="post" action="{{ url('admin/section/about/update') }}">
							<input type="hidden" name="_method" value="put">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id" value="{{ $about[0]->id }}">

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>About Title</strong></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="about_title" value="{{ $about[0]->about_title }}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>About Subtitle</strong></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="about_subtitle" value="{{ $about[0]->about_subtitle }}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><strong>About Text</strong></label>
								<div class="col-md-8">
									<textarea rows="4" class="form-control" name="about_text">{{$about[0]->about_text}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="submit" class="btn btn-primary">
										Save
									</button>
								</div>
							</div>
						</form>
					</div>
			</div>
	    </div>
	</div>
@endsection
