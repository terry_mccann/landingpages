<section id="menu" class="module">
		<div class="container">

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="module-header wow fadeInUp">
						<p class="module-title">{{$menu[0]->menu_title}}</p>
						<p class="module-subtitle">{{$menu[0]->menu_subtitle}}</p>
					</div>
				</div>
			</div><!-- .row -->

			@foreach($menu as $m)
				<div class="col-sm-6">

					<div class="menu">
						<div class="row">
							<div class="col-sm-8">
								<h4 class="menu-title">{{ $m->menu_item_title }}</h4>
								<div class="menu-detail">{{ $m->menu_item_detail }}</div>
							</div>
							<div class="col-sm-4 menu-price-detail">
								<h4 class="menu-price">${{ $m->menu_item_price }}</h4>
							</div>
						</div>
					</div>

				</div><!-- .col-sm-6 -->
			@endforeach
				

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3 long-up text-center">
					<a href="" class="btn btn-custom-1">Full Menu PDF</a>
				</div>
			</div><!-- .row -->

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="divider">
						<img src="assets/images/divider-down.svg" alt="">
					</div>
				</div>
			</div><!-- .row -->

		</div><!-- .container -->
	</section>