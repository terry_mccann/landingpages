<!-- About start -->

<section id="about" class="module">
	<div class="container">

		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="module-header wow fadeInUp">
					<p class="module-title">{{ $about[0]->about_title }}</p>
					<p class="module-subtitle">{{ $about[0]->about_subtitle }}</p>
				</div>
			</div>
		</div><!-- .row -->

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				<p>{{ $about[0]->about_text }}</p>
			</div>
		</div><!-- .row -->

		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="divider">
					<img src="{{ $about[0]->about_divider_image }}" alt="">
				</div>
			</div>
		</div><!-- .row -->

	</div><!-- .container -->
</section>

<!-- About end -->

<!-- Callout section start -->

<section class="callout" style="background-image: url({{ $about[0]->callout_background_image }});">
	<div class="container">

		<div class="row">
			<div class="col-sm-2 col-sm-offset-5 text-center long-down">
				<img src="{{ $about[0]->callout_divider_image }}" alt="">
			</div>
		</div><!-- .row -->

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h2 class="callout-text">{{ $about[0]->callout_text }}</h2>
			</div>
		</div><!-- .row -->

	</div><!-- .container -->
</section>

<!-- Callout section end -->