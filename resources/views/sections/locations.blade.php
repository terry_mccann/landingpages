<!-- Locations start -->

<section id="locations" class="module">
	<div class="container">

		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="module-header wow fadeInUp">
					<p class="module-title">{{ $locations[0]->location_title }}</p>
					<p class="module-subtitle">{{ $locations[0]->location_subtitle }}</p>
				</div>
			</div>
		</div><!-- .row -->

		<div class="row row-centered">
			@foreach($locations as $l)
			<div class="col-sm-3 col-centered">
				<div class="iconbox">
					<div class="iconbox-body equal-height">
						<div class="iconbox-icon"><span class="icon-pin"></span></div>
						<div class="iconbox-text">
							<h3 class="iconbox-title">{{ $l->location_header }}</h3>
							<div class="iconbox-desc">
								<address>
								  <strong>{{ $l->location_name }}</strong><br>
								  {{ $l->location_address }}<br>
								  {{ $l->location_city }}, {{ $l->location_state }} {{ $l->location_zip }}<br>
								  <abbr title="Phone">P:</abbr> {{ $l->location_phone }}
								</address>
							</div>
							<br>
							<button type="button" class="btn btn-info">Order Online</button><br>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div><!-- .row -->

		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="divider">
					<img src="{{ $locations[0]->location_divider_image }}" alt=""><br><br>
				</div>
			</div>
		</div><!-- .row -->

	</div><!-- .container -->
</section>

<section class="callout" style="background-image: url({{ $locations[0]->callout_background_image }});">
	<div class="container">

		<div class="row">
			<div class="col-sm-2 col-sm-offset-5 text-center long-down">
				<img src="{{ $locations[0]->callout_divider_image }}" alt="">
			</div>
		</div><!-- .row -->

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h2 class="callout-text">{{ $locations[0]->callout_text }}</h2>
			</div>
		</div><!-- .row -->

	</div><!-- .container -->
</section>

<!-- Callout section end -->